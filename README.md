# Media PostGrid UI

Project for the UI development of the *Media PostGrid* plugin.<br>
The project should contain everything to run the UI locally.

If `python` is available, start

```bash
python -m http.server
```

inside the project's directory.<br>
This should start a simple HTTP server on port 8000.
